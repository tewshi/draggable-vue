<!-- @format -->

# draggable

## Live demo

https://brave-ritchie-3cde46.netlify.app/

## Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build
```
